<?php
include '../koneksi.php'; //agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <title>Form Data Dosen</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <h3 class="text-center">Tambah Data Kelas</h3>
                <form method="POST" action="tambah_proses.php" enctype="multipart/form-data">

                    <div class="col-md-12">
                        <input class="form-control" type="text" name="nama_kelas" placeholder="Nama Kelas" required>
                    </div>

                    <div class="col-md-12">
                        <input class="form-control" type="text" name="prodi" placeholder="Prodi" required>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="fakultas" placeholder="Fakultas" required>

                    </div>

                    <div class="row ">
                        <div class="col-md-3">
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-button mt-3">
                                <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-button mt-3">
                                <a href="index.php" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>
</body>

</html>