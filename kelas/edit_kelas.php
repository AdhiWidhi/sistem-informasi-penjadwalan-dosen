<?php
// memanggil file koneksi.php untuk membuat koneksi
include '../koneksi.php';

// mengecek apakah di url ada nilai GET id_kelas
if (isset($_GET['id_kelas'])) {
    // ambil nilai id_kelas dari url dan disimpan dalam variabel $id_kelas
    $id_kelas = ($_GET["id_kelas"]);

    // menampilkan data dari database yang mempunyai id_kelas=$id_kelas
    $query = "SELECT * FROM kelas WHERE id_kelas='$id_kelas'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);
    // apabila data tidak ada pada database maka akan dijalankan perintah ini
    if (!count($data)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    // apabila tidak ada data GET id_kelas pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id_kelas.');window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <title>Form Data Kelas</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <h3>Edit data <?php echo $data['nama_kelas']; ?></h3>
                <form method="POST" action="edit_proses.php" enctype="multipart/form-data" class="text-white">

                    <!-- menampung nilai id_kelas produk yang akan di edit -->
                    <input name="id_kelas" value="<?php echo $data['id_kelas']; ?>" hidden />
                    <div>
                        <label>Nama</label>
                        <input type="text" name="nama_kelas" value="<?php echo $data['nama_kelas']; ?>" autofocus=""
                            required="" />
                    </div>
                    <br>
                    <div>
                        <label>Prodi</label>
                        <input type="text" name="prodi" required="" value="<?php echo $data['prodi']; ?>" />
                    </div>
                    <br>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="fakultas" required="" value="<?php echo $data['fakultas']; ?>" />
                    </div>
                    <br>
                    <div class="row ">
                        <div class="col-md-3">
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-button mt-3">
                                <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-button mt-3">
                                <a href="index.php" class="btn btn-primary">Back</a>
                            </div>
                        </div>

                </form>
            </div>
        </div>
    </div>


</body>

</html>