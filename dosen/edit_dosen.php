<?php
// memanggil file koneksi.php untuk membuat koneksi
include '../koneksi.php';

// mengecek apakah di url ada nilai GET id_dosen
if (isset($_GET['id_dosen'])) {
    // ambil nilai id_dosen dari url dan disimpan dalam variabel $id_dosen
    $id_dosen = ($_GET["id_dosen"]);

    // menampilkan data dari database yang mempunyai id_dosen=$id_dosen
    $query = "SELECT * FROM dosen WHERE id_dosen='$id_dosen'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);
    // apabila data tidak ada pada database maka akan dijalankan perintah ini
    if (!count($data)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    // apabila tidak ada data GET id_dosen pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id_dosen.');window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <title>Form Data Dosen</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <h3>Edit data <?php echo $data['nama_dosen']; ?></h3>

                <form method="POST" action="proses_edit.php" enctype="multipart/form-data" class="text-white">

                    <!-- menampung nilai id_dosen produk yang akan di edit -->
                    <input name="id_dosen" value="<?php echo $data['id_dosen']; ?>" hidden />
                    <div>
                        <label>Nama</label>
                        <input type="text" name="nama_dosen" value="<?php echo $data['nama_dosen']; ?>" autofocus=""
                            required />
                    </div>
                    <div>
                        <label>NIP</label>
                        <input type="text" name="nip_dosen" value="<?php echo $data['nip_dosen']; ?>" />
                    </div>
                    <div>
                        <label>Prodi</label>
                        <input type="text" name="prodi" required value="<?php echo $data['prodi']; ?>" />
                    </div>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="fakultas" required value="<?php echo $data['fakultas']; ?>" />
                    </div>
                    <div>
                        <label>Foto</label>
                        <br>
                        <img src="gambar/<?php echo $data['foto_dosen']; ?>"
                            style="width: 120px;float: left;margin-bottom: 5px;" class="border border-5">

                        <input type="file" name="foto_dosen" class="form-control" />
                        <i style="float: left;font-size: 11px;" class="text-danger">Abaikan jika tidak merubah foto
                            dosen</i> <br>
                    </div>
                    <div class="row ">
                        <div class="col-md-3">
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-button mt-3">
                                <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-button mt-3">
                                <a href="index.php" class="btn btn-primary">Back</a>
                            </div>
                        </div>

                </form>
            </div>
        </div>
    </div>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>
</body>

</html>