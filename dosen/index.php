<?php

include '../koneksi.php' //agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include

?>
<!DOCTYPE html>
<html>

<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <title>Tabel Dosen</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <table class="table  text-white">
                    <h3 class="text-center">Data Dosen</h3>
                    <a href="tambah_dosen.php" class="btn btn-primary ">Tambah Data</a>
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>NIP</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Foto</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        // jalankan query untuk menampilkan semua data diurutkan berdasarkan id_dose 
                        $query = "SELECT * FROM dosen ORDER BY id_dosen ASC";
                        $result = mysqli_query($koneksi, $query);
                        //mengecek apakah ada error ketika menjalankan query
                        if (!$result) {
                            die("Query Error: " . mysqli_errno($koneksi) .
                                " - " . mysqli_error($koneksi));
                        }

                        //buat perulangan untuk element tabel dari data dosen
                        $no = 1; //variabel untuk membuat nomor urut
                        // hasil query akan disimpan dalam variabel $data dalam bentuk array
                        // kemudian dicetak dengan perulangan while
                        while ($row = mysqli_fetch_assoc($result)) {
                        ?>
                        <tr>
                            <td><?php echo $no; ?></td>
                            <td><?php echo $row['nama_dosen']; ?></td>
                            <td><?php echo $row['nip_dosen']; ?></td>
                            <td><?php echo $row['prodi']; ?></td>
                            <td><?php echo $row['fakultas']; ?></td>
                            <td style="text-align: center;"><img src="gambar/<?php echo $row['foto_dosen']; ?>"
                                    style="width: 120px;"></td>
                            <td>
                                <a href="edit_dosen.php?id_dosen=<?php echo $row['id_dosen']; ?>"
                                    class="btn btn-primary">Edit</a>

                                <a href="proses_hapus.php?id_dosen=<?php echo $row['id_dosen']; ?>"
                                    onclick="return confirm('Anda yakin akan menghapus data ini?')" "
                                    class=" btn btn-primary">Hapus</a>
                            </td>
                        </tr>

                        <?php
                            $no++; //untuk nomor urut terus bertambah 1
                        }
                        ?>
                    </tbody>
                </table>
                <a href="../index.php" class="btn btn-primary">Back</a>

            </div>
        </div>
    </div>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>
</body>

</html>