<?php
// memanggil file koneksi.php untuk membuat koneksi
include '../koneksi.php';

// mengecek apakah di url ada nilai GET id_jadwal
if (isset($_GET['id_jadwal'])) {
    // ambil nilai id_jadwal dari url dan disimpan dalam variabel $id_jadwal
    $id_jadwal = ($_GET["id_jadwal"]);

    // menampilkan data dari database yang mempunyai id_jadwal=$id_jadwal
    $query = "SELECT * FROM jadwal_kelas WHERE id_jadwal='$id_jadwal'";
    $result = mysqli_query($koneksi, $query);
    // jika data gagal diambil maka akan tampil error berikut
    if (!$result) {
        die("Query Error: " . mysqli_errno($koneksi) .
            " - " . mysqli_error($koneksi));
    }
    // mengambil data dari database
    $data = mysqli_fetch_assoc($result);
    // apabila data tidak ada pada database maka akan dijalankan perintah ini
    if (!count($data)) {
        echo "<script>alert('Data tidak ditemukan pada database');window.location='index.php';</script>";
    }
} else {
    // apabila tidak ada data GET id_jadwal pada akan di redirect ke index.php
    echo "<script>alert('Masukkan data id_jadwal.');window.location='index.php';</script>";
}
?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <title>Form Data Kelas</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <h3>Edit Jadwal <?php echo $data['matakuliah']; ?></h3>
                <form method="POST" action="editProses.php" enctype="multipart/form-data" class="text-white">

                    <!-- menampung nilai id_jadwal produk yang akan di edit -->
                    <input name="id_jadwal" value="<?php echo $data['id_jadwal']; ?>" hidden />
                    <select name="id_dosen" id="id_dosen" class="form-select" required>
                        <option value="">Pilih ID Dosen</option>
                        <?php
                        $queryDosen = mysqli_query(
                            $koneksi,
                            "SELECT * FROM dosen "
                        ) or die(mysqli_error($koneksi));
                        while ($data_dosen = mysqli_fetch_array($queryDosen)) {
                            echo '<option value="' . $data_dosen['id_dosen'] . '">' . $data_dosen['id_dosen'] . '</option>';
                        }
                        ?>
                    </select>
                    <br>
                    <select name="id_kelas" id="id_kelas" class="form-select" required>
                        <option value="">Pilih ID Kelas</option>
                        <?php
                        $queryDosen = mysqli_query(
                            $koneksi,
                            "SELECT * FROM kelas "
                        ) or die(mysqli_error($koneksi));
                        while ($data_dosen = mysqli_fetch_array($queryDosen)) {
                            echo '<option value="' . $data_dosen['id_kelas'] . '">' . $data_dosen['id_kelas'] . '</option>';
                        }
                        ?>
                    </select>
                    <br>
                    <div>
                        <label>Jadwal</label>
                        <input type="text" name="jadwal" required="" value="<?php echo $data['jadwal']; ?>" />
                    </div>
                    <br>
                    <div>
                        <label>Fakultas</label>
                        <input type="text" name="matakuliah" required="" value="<?php echo $data['matakuliah']; ?>" />
                    </div>

                    <div class="row ">
                        <div class="col-md-3">
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-button mt-3">
                                <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-button mt-3">
                                <a href="index.php" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


</body>

</html>