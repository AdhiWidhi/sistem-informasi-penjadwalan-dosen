<?php
include '../koneksi.php';
?>

<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <title>Tabel Jadwal Kelas</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <table class="table  text-white">
                    <h3 class="text-center">Data Kelas</h3>
                    <a href="add.php" class="btn btn-primary ">Tambah Data</a>
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>ID_Dosen</th>
                            <th>id_kelas</th>
                            <th>Jadwal</th>
                            <th>Matakuliah</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php

                        $query = mysqli_query(
                            $koneksi,
                            "SELECT * FROM jadwal_kelas 
                        INNER JOIN dosen ON jadwal_kelas.id_dosen = dosen.id_dosen 
                        INNER JOIN kelas ON jadwal_kelas.id_kelas = kelas.id_kelas"
                        ) or die(mysqli_error($koneksi));

                        $no = 1;
                        foreach ($query as $row) :
                        ?>

                        <tr>
                            <td><?= $no++; ?></td>
                            <td><?= $row['nama_dosen']; ?></td>
                            <td><?= $row['nama_kelas']; ?></td>
                            <td><?= $row['jadwal']; ?></td>
                            <td><?= $row['matakuliah']; ?></td>
                            <td> <a href="edit.php?id_jadwal=<?php echo $row['id_jadwal']; ?>"
                                    class="btn btn-primary">Edit</a>
                                <a href="deleteProses.php?id_jadwal=<?php echo $row['id_jadwal']; ?>"
                                    onclick="return confirm('Anda yakin akan menghapus data ini?')"
                                    class="btn btn-primary">Hapus</a>
                            </td>
                        </tr>

                        <?php endforeach; ?>
                    </tbody>
                </table>
                <a href="../index.php" class="btn btn-primary">Back</a>
            </div>
        </div>
    </div>


</body>

</html>