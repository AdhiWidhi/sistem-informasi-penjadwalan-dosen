<?php
include '../koneksi.php'; //agar index terhubung dengan database, maka koneksi sebagai penghubung harus di include

?>
<!DOCTYPE html>
<html>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="../style.css">
    <title>Form Data Dosen</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <h3 class="text-center">Tambah Data Jadwal</h3>
                <form method="POST" action="addProses.php" enctype="multipart/form-data" class="text-white">
                    <label>ID Dosen</label>
                    <select name="id_dosen" id="id_dosen" class="form-select" required>
                        <option value="">Pilih ID Dosen</option>
                        <?php
                        $queryDosen = mysqli_query(
                            $koneksi,
                            "SELECT * FROM dosen "
                        ) or die(mysqli_error($koneksi));
                        while ($data_dosen = mysqli_fetch_array($queryDosen)) {
                            echo '<option value="' . $data_dosen['id_dosen'] . '">' . $data_dosen['id_dosen'] . '</option>';
                        }
                        ?>
                    </select>
                    <br>
                    <label>ID Kelas</label>
                    <select name="id_kelas" id="id_kelas" class="form-select" required>
                        <option value="">Pilih ID Kelas</option>
                        <?php
                        $queryDosen = mysqli_query(
                            $koneksi,
                            "SELECT * FROM kelas "
                        ) or die(mysqli_error($koneksi));
                        while ($data_dosen = mysqli_fetch_array($queryDosen)) {
                            echo '<option value="' . $data_dosen['id_kelas'] . '">' . $data_dosen['id_kelas'] . '</option>';
                        }
                        ?>
                    </select>
                    <br>
                    <div>
                        <label>Jadwal</label>
                        <input type="date" name="jadwal" required="" class="form-control" />
                    </div>
                    <br>
                    <div>
                        <label>Matakuliah</label>
                        <input type="text" name="matakuliah" required="" class="form-control"
                            placeholder="Matakuliah" />
                    </div>
                    <br>
                    <div class="row ">
                        <div class="col-md-3">
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-button mt-3">
                                <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-button mt-3">
                                <a href="index.php" class="btn btn-primary">Back</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


</body>

</html>